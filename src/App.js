import React, { useEffect, useState } from 'react'
import Dashboard from './pages/Dashboard'
import CardDetails from './pages/components/CardLayouts/CardDetails'
import { Container, Row, Col, Button } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'react-toastify/dist/ReactToastify.css'
import {
  BrowserRouter,
  Link,
  Routes,
  Route,
  Router,
  NavLink,
} from 'react-router-dom'
import './index.css'
import { GrNext } from 'react-icons/gr'
import SortToggle from './pages/components/toggleTypes/SortToggle'
import Breadcrumbs from './pages/components/Breadcrumbs/Breadcrumbs'
import DeckDetails from './pages/components/DeckLayouts/DeckDetails'
import Login from './pages/components/Screens/Login'
import { toast } from 'react-toastify'
const _ = require('lodash')

function App() {
  let characterName = localStorage.getItem('card')
  const [breadcrumbItem, setBreadcrumbItem] = useState('')
  const [screenState, setScreenState] = useState(true)
  const [selectedDeck, setSelectedDeck] = useState(0)
  const [decks, setDecks] = useState([])
  const [localStorageDecks, setLocalStorageDecks] = useState([])
  //   const [cardsInDeck, setCardsInDeck] = useState([])

  /* useEffect(() => {
    if (characterName) {
      setBreadcrumbItem(JSON.parse(characterName))
    } else {
      setBreadcrumbItem('')
    }
  }, [characterName]) */
  // console.log(breadcrumbItem, 'bci')
  // console.log(characterName, 'cn')
  // console.log(screenState, 'screen state on App')
  const resetCharacter = (e) => {
    localStorage.removeItem('card')
  }

  let deckInformation = localStorage.getItem('deckName')
  let deckData
  if (typeof deckData !== 'undefined') {
    deckData = JSON.parse(deckInformation)
  } else {
    deckData = []
  }

  //   console.log(selectedDeck, 'selectedDeck id from deck on app')

  /* seEffect(() => {
    setCardsInDeck(
      deckData
        ?.filter((f) => f.id === selectedDeck)
        ?.map((deckCard) =>
          deckCard.cards.map((card) => {
            return card
          }),
        ),
    )
  }, [selectedDeck]) */

  let cardsInDeck = [...localStorageDecks]
    ?.filter((f) => f.id === selectedDeck.id)
    ?.map((deckCard) =>
      deckCard.cards?.map((card) => {
        return card
      })
    )

  //   console.log(decks, 'this is deckData on card')

  const removeDeck = async (data) => {
    let updatedDeck = _.reject([...localStorageDecks], data)
    localStorage.setItem('deckName', JSON.stringify(updatedDeck))
    setLocalStorageDecks([...updatedDeck])
    toast.info(`${data?.name} has been deleted`)
  }

  const removeCard = (data, transferData = null) => {
    let newCardsList = transferData ? transferData : [...localStorageDecks]

    let updatedCard = cardsInDeck[0]
      ?.map((x) => x)
      ?.filter((f) => f?.url !== data?.url)

    newCardsList = newCardsList.map((mappedDeck) =>
      mappedDeck?.id === selectedDeck?.id
        ? {
            ...mappedDeck,
            cards: updatedCard,
          }
        : { ...mappedDeck }
    )
    localStorage.setItem('deckName', JSON.stringify(newCardsList))
    setLocalStorageDecks([...newCardsList])
    // console.log(newCardsList, 'card removed from deck')
    toast.info(`${data?.name} has been deleted`)
  }

  const getData = async () => {
    let x = await JSON.parse(localStorage.getItem('deckName'))
    // console.log(x, 'this is x on card')
    setLocalStorageDecks([...x])
  }

  useEffect(() => {
    if (localStorageDecks.length < 1) {
      getData()
      //   console.log(localStorageDecks, 'this is decks on card')
    }
  }, [])

  // console.log(localStorageDecks, 'decks')

  /*       const assignToDeck = (obj) => {
    console.log(obj, 'obj')
    console.log(obj?.cards?.length, 'cards length')

    let x = [...decks]
    let p = findUsedDeck(obj)
    console.log(p, 'this is p')
    x = x.map((mappedDeck) =>
      mappedDeck.name === obj.name
        ? {
            ...mappedDeck,
            cards:
              mappedDeck?.cards?.length > 0
                ? [...mappedDeck?.cards, characters]
                : [characters],
          }
        : { ...mappedDeck },
    )
    setDecks([...x])
    toast.success(`${characters.name} has been added to ${obj.name}`)
  }

  useEffect(() => {
    if (decks?.length > 0) {
      localStorage.setItem('deckName', JSON.stringify(decks))
    }
  }, [decks]) */

  const transferCard = (data, deckToBeTransferred) => {
    let x = [...localStorageDecks]
    x = x?.map((mappedDeck) =>
      mappedDeck?.id === deckToBeTransferred?.id
        ? {
            ...mappedDeck,
            cards:
              mappedDeck?.cards?.length > 0
                ? [...mappedDeck?.cards, data]
                : [data],
          }
        : {
            ...mappedDeck,
          }
    )

    // localStorage.setItem('deckName', JSON.stringify(x))
    // console.log(x, 'x after map')
    // setLocalStorageDecks([...x])
    removeCard(data, x)
    toast.success(
      `${data?.name} has been transferred to ${deckToBeTransferred?.name}`
    )
  }

  /* const cardsInDeck = () => {
        selectedDeck.map((s) =>
      s.cards.map((c) => {
        return c
      }),
    )
    } */

  /* const submitHandler = (e) => {
      e.preventDefault()
  
      if (username.length === 0 || pwd.length === 0) {
        return
      }
      window.localStorage.setItem('isLogged in', true)
      window.localStorage.setItem('username', username)
      navigate('/')
      setPwd('')
      setUsername('')
    } */

  //   console.log(decks, 'this is the deck in App')
  //   console.log(localStorageDecks, 'this is the localstoragedeck in App')

  return (
    <BrowserRouter>
      <Container className=''>
        <Row className={'pt-5 border-bottom-2 align-items-center mx-auto'}>
          <Col xs sm={12} className='d-flex flex-nowrap px-0'>
            <Col className={'mb-2'}>
              <div className='d-flex flex-nowrap'>
                <div className={'me-2'} style={{ whiteSpace: 'nowrap' }}>
                  <NavLink
                    // variant={'light'}
                    to={'/cards'}
                    onClick={() => {
                      if (screenState === false) {
                        setScreenState(!screenState)
                      }
                    }}
                    className={(navData) =>
                      navData.isActive
                        ? 'btn btn-light btn-sm btn-light__light text-decoration-none'
                        : 'btn btn-sm btn-secondary btn-secondary__light text-decoration-none'
                    }
                  >
                    <img
                      src='/Interview Assets/Card.svg'
                      alt='Card.svg for all cards button'
                      className='me-2'
                    />
                    All Cards
                  </NavLink>
                </div>
                <div style={{ whiteSpace: 'nowrap' }}>
                  <NavLink
                    // variant={'secondary'}
                    onClick={() => setScreenState(!screenState)}
                    to={'/decks'}
                    className={(navData) =>
                      navData.isActive
                        ? 'btn btn-light btn-sm btn-light__light text-decoration-none'
                        : 'btn btn-sm btn-secondary btn-secondary__light text-decoration-none'
                    }
                  >
                    <img
                      src='/Interview Assets/Deck.svg'
                      alt='Deck.svg for decks button'
                      className='me-2'
                    />
                    Decks
                  </NavLink>
                </div>
              </div>
            </Col>
            <Col
              className={'text-center d-none d-sm-none d-md-none d-lg-block'}
            >
              <h4>
                <span>SW</span>
                <span className={'text-secondary'}>-API Deck Builder</span>
              </h4>
            </Col>
            <Col className={'d-flex justify-content-end mb-2'}>
              <label
                htmlFor='userName'
                className='border-darken-1 py-1 px-2 rounded'
              >
                Jevon Hayden
              </label>
            </Col>
          </Col>
        </Row>
        <Row className={'mt-2pt align-items-center'}>
          <Col>
            <Breadcrumbs
              breadcrumbItem={breadcrumbItem}
              resetCharacter={resetCharacter}
              screen={screenState}
            />
          </Col>
        </Row>
        <Routes>
          <Route index exact element={<Login />} />
          <Route
            path='/cards'
            exact
            element={
              <Dashboard
                item={setBreadcrumbItem}
                screen={screenState}
                removeDeck={removeDeck}
                deckData={[...deckData]}
                setSelectedDeck={setSelectedDeck}
                localStorageDecks={[...localStorageDecks]}
                setLocalStorageDecks={setLocalStorageDecks}
              />
            }
          />
          <Route
            path='/decks'
            exact
            element={
              <Dashboard
                item={setBreadcrumbItem}
                screen={screenState}
                removeDeck={removeDeck}
                deckData={[...deckData]}
                setSelectedDeck={setSelectedDeck}
                localStorageDecks={[...localStorageDecks]}
                setLocalStorageDecks={setLocalStorageDecks}
              />
            }
          />
          <Route
            path='/card-details'
            exact
            element={<CardDetails item={setBreadcrumbItem} />}
          />
          <Route
            path='/deck-details'
            exact
            element={
              <DeckDetails
                screen={screenState}
                item={setBreadcrumbItem}
                removeCard={removeCard}
                deckData={[...deckData]}
                selectedDeck={selectedDeck}
                cardsInDeck={cardsInDeck}
                localStorageDecks={[...localStorageDecks]}
                setLocalStorageDecks={setLocalStorageDecks}
                transferCard={transferCard}
              />
            }
          />
        </Routes>
      </Container>
    </BrowserRouter>
  )
}

export default App
