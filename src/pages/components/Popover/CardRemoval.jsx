import React from 'react'
import { Button, Col, OverlayTrigger, Popover, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'

const CardRemoval = ({ removeCard, card }) => {
  return (
    <>
      <OverlayTrigger
        trigger='focus'
        placement={'bottom-end'}
        overlay={
          <Popover className='border-2 bg-main'>
            <Popover.Header>
              <Col className='d-flex justify-content-center'>
                {`Delete ${card?.name}?`}
              </Col>
            </Popover.Header>
            <Popover.Body className='bg-main pt-0 pb-2 px-0 rounded'>
              <div className='mx-2'>
                <Row className=' py-1'>
                  <Col>
                    {/* <input
                      type='text'
                      name='deck_name'
                      id=''
                      className='form-control'
                      placeholder='Enter Deck Name'
                    /> */}
                    <Button
                      variant='link'
                      size='sm'
                      className='px-4 py-0 mt-1 text-decoration-none text-body delete-cancel popover-selection shadow'
                    >
                      No
                    </Button>
                  </Col>
                  <Col>
                    {/* <input
                      type='text'
                      name='deck_name'
                      id=''
                      className='form-control'
                      placeholder='Enter Deck Name'
                    /> */}
                    <Button
                      size='sm'
                      className='px-4 py-0 mt-1 delete-confirm popover-selection shadow'
                      onClick={() => {
                        removeCard(card)
                      }}
                    >
                      Yes
                    </Button>
                  </Col>
                </Row>
              </div>
            </Popover.Body>
          </Popover>
        }
      >
        <Button variant='link' className='py-1 text-decoration-none btn-light'>
          <img
            src='/Interview Assets/Remove.svg'
            alt=''
            className='rounded ms-auto svg-greyscale'
          />
        </Button>
      </OverlayTrigger>
      {/* <ToastContainer
        position='top-right'
        autoClose={2500}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        toastClassName={'toast-secondary'}
        progressClassName={'toast-progress-bar'}
        limit={2}
      /> */}
    </>
  )
}

export default CardRemoval
