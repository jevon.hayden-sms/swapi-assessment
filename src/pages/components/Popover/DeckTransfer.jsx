import React from 'react'
import { OverlayTrigger, Popover, Row, Col, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'

const DeckTransfer = ({
  comp,
  transferDeckName,
  transferCard,
  card,
  decks,
  setDecks,
  characters,
}) => {
  return (
    <>
      <OverlayTrigger
        trigger='focus'
        placement={'bottom-end'}
        overlay={
          <Popover className='border-2 bg-main'>
            <Popover.Header className='bg-main text-secondary border-bottom-0'>
              Select a deck
              <hr className='mt-1 mb-0 line-size' />
            </Popover.Header>
            <Popover.Body className='bg-main pt-0'>
              <div className='mx-2'>
                {comp === 'deck'
                  ? transferDeckName?.map((singleItem) => (
                      <Row
                        className='bg-white rounded py-1 pe-5 mb-2'
                        key={singleItem.name}
                      >
                        <Col>
                          <Link
                            to={'#!'}
                            /* onClick={() => {
                        assignToDeck(singleItem)
                        console.log(
                          singleItem,
                          characters,
                          'singleItem & characters',
                        )
                      }} */
                            onClick={() => {
                              // console.log(singleItem, 'singleItem')
                              transferCard(card, singleItem)
                            }}
                            className='text-decoration-none text-body'
                          >
                            {singleItem.name}
                          </Link>
                        </Col>
                      </Row>
                    ))
                  : decks?.map((singleItem) => (
                      <Row
                        className='bg-white rounded py-1 pe-5 mb-2'
                        key={singleItem.name}
                      >
                        <Col>
                          <Link
                            to={'#!'}
                            /* onClick={() => {
                        assignToDeck(singleItem)
                        console.log(
                          singleItem,
                          characters,
                          'singleItem & characters',
                        )
                      }} */
                            className='text-decoration-none text-body'
                          >
                            {singleItem.name}
                          </Link>
                        </Col>
                      </Row>
                    ))}
              </div>
            </Popover.Body>
          </Popover>
        }
      >
        <Button
          variant='link'
          className='py-1 mx-2 btn-light text-decoration-none'
        >
          <img
            src='/Interview Assets/Move.svg'
            alt=''
            className='rounded ms-auto svg-greyscale'
          />
        </Button>
      </OverlayTrigger>
      {/* <ToastContainer
        position="top-right"
        autoClose={2500}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        toastClassName={'toast-secondary'}
        progressClassName={'toast-progress-bar'}
      /> */}
    </>
  )
}

export default DeckTransfer
