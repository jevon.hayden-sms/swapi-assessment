import React from 'react'
import { Button, Col, OverlayTrigger, Popover, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'

const DeckDeletion = ({ data, faction, removeDeck }) => {
  const returnId = (id) => {
    let icon
    switch (id) {
      case 0:
        icon = faction.none.watermark
        break
      case 1:
        icon = faction.rebel.watermark
        break
      case 2:
        icon = faction.jedi.watermark
        break
      case 3:
        icon = faction.galatic.watermark
        break
      default:
      // code block
    }
    return icon
  }

  return (
    <>
      <OverlayTrigger
        trigger='focus'
        placement={'bottom-end'}
        overlay={
          <Popover className='border-2 bg-main'>
            <Popover.Header>
              <Col className='d-flex justify-content-center'>
                {`Delete ${data?.name}?`}
              </Col>
            </Popover.Header>
            <Popover.Body className='bg-main pt-0 pb-2 px-0 rounded'>
              <div className='mx-2'>
                <Row className=' py-1'>
                  <Col>
                    {/* <input
                      type='text'
                      name='deck_name'
                      id=''
                      className='form-control'
                      placeholder='Enter Deck Name'
                    /> */}
                    <Button
                      variant='link'
                      size='sm'
                      className='px-4 py-0 mt-1 text-decoration-none text-body delete-cancel popover-selection shadow'
                    >
                      No
                    </Button>
                  </Col>
                  <Col>
                    {/* <input
                      type='text'
                      name='deck_name'
                      id=''
                      className='form-control'
                      placeholder='Enter Deck Name'
                    /> */}
                    <Button
                      size='sm'
                      className='px-4 py-0 mt-1 delete-confirm popover-selection shadow'
                      onClick={() => {
                        // removeCard(card)
                      }}
                    >
                      Yes
                    </Button>
                  </Col>
                </Row>
              </div>
            </Popover.Body>
          </Popover>
        }
      >
        <>
          <div>
            <img
              src={returnId(data.factionId)}
              alt=''
              className='icon-watermark'
            />
          </div>
          <Button
            variant='link'
            className='py-1 text-decoration-none btn-deck-dark'
            onClick={() => {
              removeDeck(data)
              // console.log(data.id, 'deck id')
            }}
            style={{ zIndex: '10' }}
          >
            <img
              src='/Interview Assets/Remove.svg'
              alt=''
              className='rounded ms-auto svg-white'
            />
          </Button>
        </>
      </OverlayTrigger>
      <ToastContainer
        position='top-right'
        autoClose={2500}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        toastClassName={'toast-secondary'}
        progressClassName={'toast-progress-bar'}
      />
    </>
  )
}

export default DeckDeletion
