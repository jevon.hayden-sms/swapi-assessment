import React, { useEffect } from 'react'
import { useState } from 'react'
import { Button, Col, OverlayTrigger, Popover, Row } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { toast, ToastContainer } from 'react-toastify'
import { v4 as uuidv4 } from 'uuid'

const DeckCreation = ({ faction, deck, setDeck }) => {
  // console.log(faction)

  const [text, setText] = useState('')
  //   const [] = useState([])
  //   const [count, setCounter] = useState(1)
  const [factionSelected, setFactionSelected] = useState('')
  const [factionId, setFactionId] = useState(0)

  const onSubmit = (e) => {
    e.preventDefault()
    if (e.key === 'Enter') {
      //   console.log(text.length, 'textlength')

      if (text.length > 0) {
        // setCounter(count + 1)

        // console.log(count, 'deck counter 3')
        setDeck((newDeck) => [
          ...newDeck,
          { id: uuidv4(), factionId: factionId, name: text },
        ])
        toast.success(`${text} has been created`)
      }
    }
  }

  useEffect(() => {
    if (deck?.length > 0) {
      localStorage.setItem('deckName', JSON.stringify(deck))
      //   console.log(count, 'deck counter')
    }
  })

  //   console.log(count, 'deck counter 2')

  return (
    <>
      <OverlayTrigger
        trigger='click'
        placement={'bottom-end'}
        overlay={
          <Popover className='border-2 bg-main'>
            <Popover.Header className='bg-main text-secondary border-bottom-0'>
              <Row className='align-items-start'>
                <Col xs={4}>Faction</Col>
                <Col className='d-flex justify-content-between'>
                  <Button
                    variant='link'
                    className={
                      factionSelected === faction?.rebel?.name
                        ? 'btn-light p-1'
                        : 'btn-faction__light p-1'
                    }
                    size='sm'
                    onClick={() => {
                      setFactionSelected(faction?.rebel?.name)
                      setFactionId(faction?.rebel?.id)
                    }}
                  >
                    <img
                      src={faction?.rebel?.icon}
                      alt=''
                      className={
                        factionSelected === faction?.rebel?.name
                          ? ''
                          : 'svg-grayscale'
                      }
                    />
                  </Button>
                  <Button
                    variant='link'
                    className={
                      factionSelected === faction?.jedi?.name
                        ? 'btn-light p-1'
                        : 'btn-faction__light p-1'
                    }
                    size='sm'
                    onClick={() => {
                      setFactionSelected(faction?.jedi?.name)
                      setFactionId(faction?.jedi?.id)
                    }}
                  >
                    <img
                      src={faction?.jedi?.icon}
                      alt=''
                      className={
                        factionSelected === faction?.jedi?.name
                          ? ''
                          : 'svg-grayscale'
                      }
                    />
                  </Button>
                  <Button
                    variant='link'
                    className={
                      factionSelected === faction?.galatic?.name
                        ? 'btn-light p-1'
                        : 'btn-faction__light p-1'
                    }
                    size='sm'
                    onClick={() => {
                      setFactionSelected(faction?.galatic?.name)
                      setFactionId(faction?.galatic?.id)
                      //   setCounter(count + 1)
                    }}
                  >
                    <img
                      src={faction?.galatic?.icon}
                      alt=''
                      className={
                        factionSelected === faction?.galatic?.name
                          ? ''
                          : 'svg-grayscale'
                      }
                    />
                  </Button>

                  <Button
                    variant='link'
                    className={
                      factionSelected === faction?.none?.name
                        ? 'btn-light p-1'
                        : 'btn-faction__light p-1'
                    }
                    size='sm'
                    onClick={() => {
                      setFactionSelected(faction?.none?.name)
                      setFactionId(faction?.none?.id)
                      //   setCounter(count + 1)
                    }}
                  >
                    <img
                      src={faction?.none?.icon}
                      alt=''
                      className={
                        factionSelected === faction?.none?.name
                          ? ''
                          : 'svg-grayscale'
                      }
                    />
                  </Button>
                </Col>
                <hr className='bg-transparent' />
              </Row>
            </Popover.Header>
            <Popover.Body className='bg-main pt-0 px-0'>
              <div className='mx-2'>
                <Row className=' py-1'>
                  <Col>
                    {/* <input
                      type='text'
                      name='deck_name'
                      id=''
                      className='form-control'
                      placeholder='Enter Deck Name'
                    /> */}
                    <div className='input-data'>
                      <input
                        type='text'
                        name=''
                        id=''
                        value={text}
                        // placeholder='Enter Deck Name'
                        required
                        onChange={(e) => setText(e.target.value)}
                        onKeyUp={(e) => onSubmit(e)}
                      />
                      {/* <div className='underline'></div> */}
                      <label htmlFor=''>Deck Name</label>
                    </div>
                  </Col>
                </Row>
              </div>
            </Popover.Body>
          </Popover>
        }
      >
        <Button variant='link' className='p-0 text-decoration-none'>
          <img
            src='/Interview Assets/Add.svg'
            alt=''
            className='rounded ms-auto'
          />
        </Button>
      </OverlayTrigger>
      <ToastContainer
        position='top-right'
        autoClose={2500}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        toastClassName={'toast-secondary'}
        progressClassName={'toast-progress-bar'}
      />
    </>
  )
}

export default DeckCreation
