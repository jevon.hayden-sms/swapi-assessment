import React, { useEffect } from 'react'
import { useState } from 'react'
import { Button, Col, OverlayTrigger, Popover, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify'

const DeckAssignment = ({ decks, setDecks, characters, comp }) => {
  /* 
    run map on a deck (map di deck dem)
    JSON stringify each deck
    search the string made if it contains the card being looked for
    return each deck id
    inverted filter
    
     */

  const findUsedDeck = (obj) => {
    let newDeck = [...decks]
    let deckString

    newDeck = newDeck.map((mappedDeck) => {
      deckString = JSON.stringify(mappedDeck)
      if (deckString.includes(obj?.cards?.url)) {
        return mappedDeck?.id
      }
      //   return null
    })
    // console.log(newDeck)
    return newDeck
  }

  const assignToDeck = (obj) => {
    // console.log(obj, 'obj')
    console.log(obj?.cards?.length, 'cards length')

    let x = [...decks]
    let p = findUsedDeck(obj)
    // console.log(p, 'this is p')
    x = x.map((mappedDeck) =>
      mappedDeck.name === obj.name
        ? {
            ...mappedDeck,
            cards:
              mappedDeck?.cards?.length > 0
                ? [...mappedDeck?.cards, characters]
                : [characters],
          }
        : { ...mappedDeck }
    )
    setDecks([...x])
    toast.success(`${characters.name} has been added to ${obj.name}`)
  }

  useEffect(() => {
    if (decks?.length > 0) {
      localStorage.setItem('deckName', JSON.stringify(decks))
    }
  }, [decks])

  // console.log(characters, 'characters')

  /*   useEffect(() => {
    if (localDeck.length > 0) {
      localStorage.setItem('deckName', JSON.stringify(localDeck))
      setDecks([...localDeck])
      console.log('is this running')
    }
  }, [JSON.stringify(localDeck).length]) */

  //   console.log(decks, 'decks on deck assignment')
  return (
    <>
      <OverlayTrigger
        trigger='focus'
        placement={'bottom-end'}
        overlay={
          <Popover className='border-2 bg-main'>
            <Popover.Header className='bg-main text-secondary border-bottom-0'>
              Select a deck
              <hr className='mt-1 mb-0 line-size' />
            </Popover.Header>
            <Popover.Body className='bg-main pt-0'>
              <div className='mx-2'>
                {decks?.map((singleItem) => (
                  <Row
                    className='bg-white rounded py-1 pe-5 mb-2'
                    key={singleItem.name}
                  >
                    <Col>
                      <Link
                        to={'#!'}
                        onClick={() => {
                          assignToDeck(singleItem)
                          console.log(
                            singleItem,
                            characters,
                            'singleItem & characters'
                          )
                        }}
                        className='text-decoration-none text-body'
                      >
                        {singleItem.name}
                      </Link>
                    </Col>
                  </Row>
                ))}
              </div>
            </Popover.Body>
          </Popover>
        }
      >
        <Button
          variant='link'
          className='p-0 text-decoration-none'
          onClick={() => findUsedDeck()}
        >
          <img
            src={
              comp === 'deck'
                ? '/Interview Assets/Move.svg'
                : '/Interview Assets/Add.svg'
            }
            alt=''
            className='rounded ms-auto'
          />
        </Button>
      </OverlayTrigger>
      {/* <ToastContainer
        position='top-right'
        autoClose={2500}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        toastClassName={'toast-secondary'}
        progressClassName={'toast-progress-bar'}
      /> */}
    </>
  )
}

export default DeckAssignment
