import React from 'react'
import { Button, Col, OverlayTrigger, Popover, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const CardTransfer = () => {
  return (
    <OverlayTrigger
      trigger='click'
      placement={'bottom-end'}
      overlay={
        <Popover className='border-2 bg-main'>
          <Popover.Header className='bg-main text-secondary border-bottom-0'>
            Select a deck
            <hr className='mt-1 mb-0 line-size' />
          </Popover.Header>
          <Popover.Body className='bg-main pt-0'>
            <div className='mx-2'>
              <Row className='bg-white rounded py-1 pe-5'>
                <Col>
                  <Link to={'#!'} className='text-decoration-none text-body'>
                    Rebel Alliance Deck
                  </Link>
                </Col>
              </Row>
              <Row className='bg-white rounded py-1 my-2 pe-5'>
                <Col>
                  <Link to={'#!'} className='text-decoration-none text-body'>
                    Jedi Order Deck
                  </Link>
                </Col>
              </Row>
              <Row className='bg-white rounded py-1 my-2 pe-5 mb-4'>
                <Col>
                  <Link to={'#!'} className='text-decoration-none text-body'>
                    Galactic Empire Deck
                  </Link>
                </Col>
              </Row>
            </div>
          </Popover.Body>
        </Popover>
      }
    >
      <Button variant='link' className='p-0 text-decoration-none'>
        <img
          src='/Interview Assets/Move.svg'
          alt=''
          className='rounded ms-auto'
        />
      </Button>
    </OverlayTrigger>
  )
}

export default CardTransfer
