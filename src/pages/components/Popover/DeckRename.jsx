import React, { useEffect, useState } from 'react'
import { Button, Col, OverlayTrigger, Popover, Row } from 'react-bootstrap'
import { ToastContainer, toast } from 'react-toastify'

const DeckRename = ({ data, decks, setDecks }) => {
  const [text, setText] = useState('')

  const onSubmit = (e) => {
    let x = [...decks]

    e.preventDefault()
    if (e.key === 'Enter') {
      //   console.log(text.length, 'textlength')

      if (text.length > 0) {
        x = x.map((mappedDeck) =>
          mappedDeck.id === data.id
            ? {
                ...mappedDeck,
                name: text,
              }
            : { ...mappedDeck }
        )
        setDecks([...x])
        toast.success(`Deck has been renamed to: ${text}`)
      }
    }
  }

  useEffect(() => {
    if (decks?.length > 0) {
      localStorage.setItem('deckName', JSON.stringify(decks))
    }
  }, [decks])
  // console.log(data, 'data on rename')

  return (
    <>
      <OverlayTrigger
        trigger='click'
        placement={'bottom-end'}
        overlay={
          <Popover className='border-2 bg-main'>
            <Popover.Body className='bg-main py-2 px-0'>
              <div className='mx-2'>
                <Row className=' py-1'>
                  <Col>
                    {/* <input
                      type='text'
                      name='deck_name'
                      id=''
                      className='form-control'
                      placeholder='Enter Deck Name'
                    /> */}
                    <div className='input-data'>
                      <input
                        type='text'
                        name=''
                        id=''
                        value={text}
                        // placeholder='Enter Deck Name'
                        required
                        onChange={(e) => setText(e.target.value)}
                        onKeyUp={(e) => onSubmit(e)}
                      />
                      {/* <div className='underline'></div> */}
                      <label htmlFor=''>Deck Name</label>
                    </div>
                  </Col>
                </Row>
              </div>
            </Popover.Body>
          </Popover>
        }
      >
        <Button
          variant='link'
          className='py-1 text-decoration-none btn-deck-dark mx-2'
          style={{ zIndex: '1' }}
        >
          <img
            src='/Interview Assets/Edit.svg'
            alt=''
            className='rounded ms-auto svg-white'
          />
        </Button>
      </OverlayTrigger>
      <ToastContainer
        position='top-right'
        autoClose={2500}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        toastClassName={'toast-secondary'}
        progressClassName={'toast-progress-bar'}
      />
    </>
  )
}

export default DeckRename
