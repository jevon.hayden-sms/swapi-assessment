import { useState, useEffect } from 'react'
import FuzzySearch from 'fuzzy-search'
import { InputGroup, FormControl } from 'react-bootstrap'

const Search = ({ data, setSearchResults }) => {
  const [search, setSearch] = useState('')
  const searcher = new FuzzySearch(data, ['name'], {
    caseSensitive: false,
    sort: false,
  })
  const result = searcher.search(search)

  useEffect(() => {
    if (search !== '') {
      setSearchResults({ active: true, data: result })
    } else {
      setSearchResults({ active: false, data: null })
    }
  }, [search])

  return (
    <>
      <InputGroup size="sm" className="">
        <FormControl
          aria-label="Small"
          aria-describedby="inputGroup-sizing-sm"
          placeholder="Search"
          onChange={(e) => setSearch(e.target.value)}
          value={search}
          className={'border-0 search-left-radius'}
        />
        <InputGroup.Text
          id="inputGroup-sizing-sm"
          className="bg-white search-right-radius border-0"
        >
          <img
            src="/Interview Assets/Search.svg"
            alt="Search.svg for all search textbox"
          />
        </InputGroup.Text>
      </InputGroup>
    </>
  )
}

export default Search
