import React from 'react'

const Pagination = ({ cardsPerPage, totalCards, paginate }) => {
  const pageNumbers = []

  for (let index = 1; index <= Math.ceil(totalCards / cardsPerPage); index++) {
    pageNumbers.push(index)
  }

  return (
    <nav>
      <ul className="pagination">
        {pageNumbers.map((number) => (
          <li className="page-item mx-1" key={number}>
            <a
              href="#!"
              className="paginate-item text-decoration-none"
              onClick={() => paginate(number)}
            >
              {number}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  )
}

export default Pagination
