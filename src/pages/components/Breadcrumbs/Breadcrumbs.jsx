import React from 'react'
import { GrNext } from 'react-icons/gr'
import { Link } from 'react-router-dom'

const Breadcrumbs = ({ breadcrumbItem, resetCharacter, screen }) => {
  //   console.log(screen, 'screen on breadcrumbs')
  return (
    <>
      <Link
        to={screen === true ? '/cards' : '/decks'}
        className={'text-decoration-none breadcrumb-text-secondary'}
        onClick={(e) => resetCharacter(e.target.value)}
      >
        All {screen === true ? `Cards` : `Decks`}
      </Link>
      <GrNext className="breadcrumb-separator" />
      <Link to={'#!'} className={'text-decoration-none text-secondary ms-1'}>
        {breadcrumbItem !== '' ? `${breadcrumbItem}` : 'Select a card'}
      </Link>
    </>
  )
}

export default Breadcrumbs
