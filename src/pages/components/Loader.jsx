import React from 'react'
import { Spinner, Col, Row } from 'react-bootstrap'

const Loader = () => {
  return (
    <Row>
      <Col sm={12} className="d-flex justify-content-center">
        <Spinner
          animation="grow"
          variant="secondary"
          role="status"
          className=""
        >
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </Col>
    </Row>
  )
}

export default Loader
