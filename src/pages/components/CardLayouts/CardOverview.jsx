import axios from 'axios'
import React, { useEffect, useState } from 'react'
import {
  Button,
  Card,
  Col,
  OverlayTrigger,
  Pagination,
  Popover,
  Row,
} from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import { v4 as uuidv4 } from 'uuid'
import CardRemoval from '../Popover/CardRemoval'
import DeckAssignment from '../Popover/DeckAssignment'
import DeckTransfer from '../Popover/DeckTransfer'

const CardOverview = ({
  characters,
  species,
  decks,
  setDecks,
  card,
  comp,
  transferDeckName,
  removeCard,
  allPersons,
  transferCard,
  isDeck = false,
}) => {
  const [cardInfo, setCardInfo] = useState('')

  let cardID = uuidv4()
  let localData = {
    id: cardID,
    details: characters ?? card,
  }
  const option = (e) => {
    if ((isDeck = false)) {
      localStorage.setItem('card', JSON.stringify(localData))
    } else {
      localStorage.setItem('card', JSON.stringify(localData))
    }
  }

  return (
    <>
      <Card className={'border-0 card-rounded my-3 cards-animation'}>
        <Card.Header className={'card-header-secondary card-top-rounded'}>
          <Row className='mb-5'>
            <Col sm={5} md={5} lg={5} className={'text-white'}>
              <img
                src='/Interview Assets/Card.svg'
                alt='Card.svg icon for to symbolize all cards'
                className='icon-white'
              />
            </Col>
            <Col
              sm={7}
              md={7}
              lg={7}
              className={'mt-sm-1 d-flex justify-content-end'}
            >
              {comp === 'deck' ? (
                <>
                  <DeckTransfer
                    comp={'deck'}
                    decks={decks}
                    setDecks={setDecks}
                    characters={characters}
                    transferDeckName={transferDeckName}
                    transferCard={transferCard}
                    card={card}
                  />

                  <CardRemoval removeCard={removeCard} card={card} />
                </>
              ) : (
                <DeckAssignment
                  decks={decks}
                  setDecks={setDecks}
                  characters={characters}
                />
              )}
            </Col>
          </Row>
          <Link
            to={`/card-details/`}
            className={'text-decoration-none text-body'}
            onClick={(e) => option()}
            // onClick={() => console.log(card, 'card clicked')}
          >
            <Row>
              <Col md={12}>
                <h4
                  className={'text-white character-name overflow-name'}
                  style={{ zIndex: '1' }}
                >
                  {
                    /* card?.length > 0 && */ comp === 'deck'
                      ? card?.name
                      : characters?.name
                  }
                  {/* {
                    /* card?.length > 0 && / comp === 'deck'
                      ? card?.name?.length > 16
                        ? `${card?.name?.slice(0, 14)?.trim()?.concat('...')}`
                        : `${card?.name}`
                      : characters?.name?.length > 16
                      ? `${characters?.name
                          ?.slice(0, 14)
                          ?.trim()
                          ?.concat('...')}`
                      : `${characters?.name}`
                  } */}
                </h4>
              </Col>
            </Row>
          </Link>
        </Card.Header>
        <Card.Body>
          <Row>
            <Col
              md={12}
              className={
                'd-flex justify-content-between align-items-center border-bottom mb-3 card-body-width mx-auto'
              }
            >
              <div>
                <img
                  src={
                    comp === 'deck' && card?.gender === 'female'
                      ? '/Interview Assets/Gender-Female.svg'
                      : '/Interview Assets/Gender-Male.svg'
                  }
                  alt='Male or Female icon for gender type'
                />
                <span className={'ms-2 character-name'}>
                  {comp === 'deck' ? card?.birth_year : characters?.birth_year}
                </span>
              </div>
              <div className='character-name species-name'>
                <span>{species ?? 'unknown'}</span>
              </div>
            </Col>
          </Row>
          <Row className={'mb-2'}>
            <Col
              sm={12}
              className={
                'd-flex justify-content-between align-items-center rounded py-1 card-body-width mx-auto'
              }
              style={{ backgroundColor: '#ededed' }}
            >
              <div className='text-uppercase'>
                <img
                  src='/Interview Assets/Homeworld.svg'
                  alt='Planet icon for homeworld'
                />{' '}
                <span className={'ms-1'} style={{ fontSize: '10px' }}>
                  HOMEWORLD
                </span>
              </div>
              <div
                className='text-capitalize character-name'
                // style={{ fontSize: '14px' }}
              >
                Planet
              </div>
            </Col>
          </Row>
          <Row className={'mb-2'}>
            <Col
              sm={12}
              className={
                'd-flex justify-content-between rounded py-1 card-body-width mx-auto'
              }
              style={{ backgroundColor: '#ededed' }}
            >
              <div className='text-uppercase'>
                <img
                  src='/Interview Assets/Vehicle.svg'
                  alt='Vehicle icon for vehicles'
                />{' '}
                <span className={'ms-1'} style={{ fontSize: '10px' }}>
                  VEHICLES
                </span>
              </div>
              <div className='text-capitalize' style={{ fontSize: '14px' }}>
                {comp === 'deck'
                  ? `${card?.vehicles?.length}`
                  : `${characters?.vehicles?.length}`}
              </div>
            </Col>
          </Row>
          <Row className={'mb-2'}>
            <Col
              sm={12}
              className={
                'd-flex justify-content-between rounded py-1 card-body-width mx-auto'
              }
              style={{ backgroundColor: '#ededed' }}
            >
              <div className='text-uppercase'>
                <img
                  src='/Interview Assets/Starship.svg'
                  alt='Starship icon for Starship'
                />{' '}
                <span className={'ms-1'} style={{ fontSize: '10px' }}>
                  StarShip
                </span>
              </div>
              <div className='text-capitalize' style={{ fontSize: '14px' }}>
                {comp === 'deck'
                  ? `${card?.vehicles?.length}`
                  : `${characters?.vehicles?.length}`}
              </div>
            </Col>
          </Row>
        </Card.Body>
      </Card>
      <ToastContainer
        position='top-right'
        autoClose={2500}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        toastClassName={'toast-secondary'}
        progressClassName={'toast-progress-bar'}
        limit={2}
      />
    </>
  )
}

export default CardOverview
