import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Card, Col, Row } from 'react-bootstrap'

const CardDetails = ({ item }) => {
  const information = localStorage.getItem('card')
  const [cardInfo, setCardInfo] = useState('')
  if (cardInfo ?? cardInfo.details.name) {
    item(cardInfo.details.name)
  }

  // Retrieve information from link in person details
  const getLinkData = async (url) => {
    let newData = []
    for (let index = 0; index < url?.length; index++) {
      let response = await axios.get(url[index])
      newData.push(response?.data?.name)
    }
    return newData
  }

  useEffect(() => {
    dataStructure()
  }, [])
  // Restructure the data object
  const dataStructure = async () => {
    let data = JSON.parse(information)
    if (data?.details?.vehicles !== null) {
      let vehicle = await getLinkData(data?.details?.vehicles)
      data.details.vehicles = vehicle
    }
    if (data?.details?.starships !== null) {
      let starship = await getLinkData(data?.details?.starships)
      data.details.starships = starship
    }
    if (data?.details?.species !== null) {
      let specie = await getLinkData(data?.details?.species)
      data.details.species = specie
    }
    setCardInfo(data)
  }

  console.log(cardInfo)
  return (
    <>
      <Row className='mt-3'>
        <Col sm={12} md={12} lg={3}>
          <Card className={'border-0 card-rounded'}>
            <Card.Header className={'card-header-secondary card-top-rounded'}>
              <Row>
                <Col sm={12} md={6} lg={3} className={'mb-5 text-white'}>
                  <img
                    src='/Interview Assets/Card.svg'
                    alt='Card.svg icon for to symbolize all cards'
                    className='icon-white'
                  />{' '}
                </Col>
                <Col md={12}>
                  <h4 className={'text-white'}>{cardInfo?.details?.name}</h4>
                </Col>
              </Row>
            </Card.Header>
            <Card.Body>
              <Row>
                <Col
                  md={12}
                  className={
                    'd-flex justify-content-between border-bottom mb-3'
                  }
                >
                  <div>
                    <img
                      src={
                        cardInfo?.details?.gender === 'female'
                          ? '/Interview Assets/Gender-Female.svg'
                          : '/Interview Assets/Gender-Male.svg'
                      }
                      alt='Male or Female icon for gender type'
                    />
                    <span className={'ms-2'}>
                      {cardInfo?.details?.birth_year}
                    </span>
                  </div>
                  <div>
                    <span>{cardInfo?.details?.species}</span>
                  </div>
                </Col>
              </Row>
              <Row className={'mb-2'}>
                <Col
                  sm={12}
                  className={'d-flex justify-content-between rounded py-1'}
                  style={{ backgroundColor: '#ededed' }}
                >
                  <div className='text-uppercase'>
                    <img
                      src='/Interview Assets/Homeworld.svg'
                      alt='Planet icon for homeworld'
                    />{' '}
                    <span className={'ms-1'} style={{ fontSize: '10px' }}>
                      HOMEWORLD
                    </span>
                  </div>
                  <div className='text-capitalize' style={{ fontSize: '14px' }}>
                    Tatoonie
                  </div>
                </Col>
              </Row>
              {cardInfo?.details?.vehicles.map((vehicle, idx) => (
                <Row className={'mb-2'} key={idx}>
                  <Col
                    sm={12}
                    className={'d-flex justify-content-between rounded py-1'}
                    style={{ backgroundColor: '#ededed' }}
                  >
                    <div className='text-uppercase'>
                      <img
                        src='/Interview Assets/Vehicle.svg'
                        alt='Vehicle icon for vehicles'
                      />{' '}
                      <span className={'ms-1'} style={{ fontSize: '10px' }}>
                        VEHICLE
                      </span>
                    </div>
                    <div
                      className='text-capitalize'
                      style={{ fontSize: '14px' }}
                    >
                      {vehicle}
                    </div>
                  </Col>
                </Row>
              ))}
              {cardInfo?.details?.starships.map((starship, idx) => (
                <Row className={'mb-2'} key={idx}>
                  <Col
                    sm={12}
                    className={'d-flex justify-content-between rounded py-1'}
                    style={{ backgroundColor: '#ededed' }}
                  >
                    <div className='text-uppercase'>
                      <img
                        src='/Interview Assets/Starship.svg'
                        alt='Starship icon for Starship'
                      />{' '}
                      <span className={'ms-1'} style={{ fontSize: '10px' }}>
                        StarShip
                      </span>
                    </div>
                    <div
                      className='text-capitalize'
                      style={{ fontSize: '14px' }}
                    >
                      {starship}
                    </div>
                  </Col>
                </Row>
              ))}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </>
  )
}

export default CardDetails
