import React, { useEffect, useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import CardOverview from '../CardLayouts/CardOverview'
import Loader from '../Loader'
import Pagination from '../Pagination/Pagination'

const Card = ({
  characters,
  species,
  searchResults,
  decks,
  setDecks,
  allPersons,
}) => {
  //   console.log(characters, 'data')
  //   const deckInformation = localStorage.getItem('deckName')
  //     const deckData = JSON.parse(deckInformation)

  /*   const [decks, setDecks] = useState([])

  console.log(decks, 'decks on card')

  const getData = async () => {
    let x = JSON.parse(await localStorage.getItem('deckName'))
    console.log(x, 'this is x on card')
    setDecks([...x])
  }

  useEffect(() => {
    if (decks.length < 1) {
      getData()
      console.log(decks, 'this is decks on card')
    }
  }, [decks]) */

  const [cards, setCards] = useState([])
  const [currentPage, setCurrentPage] = useState(1)
  const [cardsPerPage, setCardsPerPage] = useState(10)

  const indexOfLastCard = currentPage * cardsPerPage
  const indexOfFirstCard = indexOfLastCard - cardsPerPage
  const currentCards = characters.slice(indexOfFirstCard, indexOfLastCard)

  useEffect(() => {
    if (allPersons.length > 0) {
      let data
      allPersons.map((m) => {
        if (m.next === `https://swapi.dev/api/people/?page=${currentPage}`) {
          data = m.results
          setCards(data)
        } else if (m.next === null) {
          data = m.results
          setCards(data)
        }
      })
    }
  }, [allPersons])

  const paginate = (pageNumber) => setCurrentPage(pageNumber)

  // console.log(currentPage)
  // console.log(cards, 'cards')
  // console.log(currentCards, 'current cards')
  // console.log(characters.length, 'length')

  return (
    <>
      <>
        {currentCards?.length > 0 && species.length > 0 ? (
          (searchResults.active ? searchResults.data : currentCards)?.map(
            (p, idx) => {
              let speciesVar
              if (species.length > 0) {
                speciesVar = species?.find((s) => p.species[0] === s.url)
              }
              return (
                <Col
                  sm={12}
                  md={6}
                  lg={3}
                  //   xl={3}
                  className='card-width'
                  key={idx}
                >
                  <CardOverview
                    characters={p}
                    species={speciesVar?.name}
                    decks={decks}
                    setDecks={setDecks}
                    allPersons={allPersons}
                  />
                </Col>
              )
            }
          )
        ) : (
          <Loader />
        )}
      </>
      <Row className='fixed-bottom-bg-secondary__light'>
        <Col>
          <Pagination
            cardsPerPage={cardsPerPage}
            totalCards={characters.length}
            paginate={paginate}
          />
        </Col>
      </Row>
    </>
  )
}

export default Card
