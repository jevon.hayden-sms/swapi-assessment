import React from 'react'
import { Card, Col, Form, FormControl, FormGroup, Row } from 'react-bootstrap'

const Login = () => {
  return (
    <>
      <Card className="min-vh-50 rounded">
        <Card.Body className="p-0 min-vh-50 rounded">
          <Row>
            <Col sm={6} md={6} lg={6} xl={6} className="bg-login">
              <h4 className="d-flex justify-content-center mt-3">
                Star Wars API Login
              </h4>
              <Row className="d-flex justify-content-center mt-3">
                <Col sm={8} md={8} lg={8} xl={8}>
                  <Form>
                    <FormGroup>
                      <FormControl type="text" name="username" />
                    </FormGroup>
                  </Form>
                </Col>
              </Row>
            </Col>
            <Col sm={6} md={6} lg={6} xl={6}>
              <img
                src="/Interview Assets/trooper.jpg"
                alt=""
                className="mw-100 mh-100"
                style={{
                  filter: 'grayscale(100%)',
                  WebkitFilter: 'grayscale(100%)',
                }}
              />
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </>
  )
}

export default Login
