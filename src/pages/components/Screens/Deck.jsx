import React, { useEffect, useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import DeckOverview from '../DeckLayouts/DeckOverview'
const _ = require('lodash')

const Deck = ({
  faction,
  deckData,
  setSelectedDeck,
  removeDeck,
  decks,
  setDecks,
}) => {
  //   const [decks, setDecks] = useState([])

  /* for (var i = 0; i < storage.length; i++) {
    console.log(storage[i], 'clicked')
    if (storage[i] === data.id) {
      // console.log('clicked also')

      storage.splice(i, 1)
      i--
      console.log(storage, 'after splice')
    }
  } */

  /* const removeDeck = async (data) => {
    let storage = JSON.parse(await localStorage.getItem('deckName'))
    console.log(storage, 'this is storage on card')
    console.log(data, 'data on deck')
    let newlist = _.reject(storage, data)
    console.log(newlist, 'this is newlist in function')
    localStorage.setItem('deckName', JSON.stringify(newlist))
    setDecks([...newlist])
  } */

  /* useEffect(() => {
    if (decks.length < 1) {
      removeDeck()
      console.log('clicked use effect')

      console.log(decks, 'this is decks on card')
    }
  }, [decks]) */

  /* useEffect(() => {
    if (decks.length > 0) {
    //   console.log(count, 'deck counter')
    }
  }) */

  //   console.log(decks, 'decks in Deck.jsx')
  //   console.log(deckData, 'deckdata in Deck.jsx')

  return (
    <>
      {decks?.length < 1 || deckData === null ? (
        <div>
          No Decks Created. Please create a Deck by pressing the Add Deck{' '}
          <span style={{ marginLeft: '-3px' }}>
            <img src='\Interview Assets\miniPlusBtn.svg' alt='' />
          </span>
          button above
        </div>
      ) : (
        <>
          {decks?.map((singleDeck) => (
            <Col
              sm={12}
              md={6}
              lg={3}
              className='card-width'
              key={singleDeck.id}
            >
              <DeckOverview
                data={singleDeck}
                decks={[...decks]}
                setDecks={setDecks}
                faction={faction}
                removeDeck={removeDeck}
                setSelectedDeck={setSelectedDeck}
              />
            </Col>
          ))}
        </>
      )}
      {/*  */}
    </>
  )
}

export default Deck
