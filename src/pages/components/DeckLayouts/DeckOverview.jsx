import React from 'react'
import {
  Button,
  Card,
  Col,
  OverlayTrigger,
  Popover,
  Row,
} from 'react-bootstrap'
import { Link } from 'react-router-dom'
import DeckDeletion from '../Popover/DeckDeletion'
import DeckRename from '../Popover/DeckRename'

const DeckOverview = ({
  data,
  faction,
  removeDeck,
  setSelectedDeck,
  decks,
  setDecks,
}) => {
  //   console.log(data, 'data on overview')
  const cardColor = (id) => {
    let color
    switch (id) {
      case 0:
        color = faction?.none?.card_class
        break
      case 1:
        color = faction?.rebel?.card_class
        break
      case 2:
        color = faction?.jedi?.card_class
        break
      case 3:
        color = faction?.galatic?.card_class
        break
      default:
      // code block
    }
    return color
  }
  /* const removeDeck = async () => {
    let storage = JSON.parse(await localStorage.getItem('deckName'))
    console.log(storage, 'this is storage')
  } */

  //   console.log(decks, 'decks in deckOverview')

  return (
    <>
      <Card className="border-0 card-rounded my-3">
        <Card.Header
          className={`card-header-${cardColor(
            data.factionId,
          )} card-top-rounded`}
        >
          <Row className="mb-2">
            <Col sm={5} md={5} lg={5} className={'text-white'}>
              <img
                src="\Interview Assets\Deck.svg"
                alt="Card.svg icon for to symbolize all cards"
                className="icon-white"
              />
            </Col>
            <Col
              sm={7}
              md={7}
              lg={7}
              className={'mt-sm-1 d-flex justify-content-end'}
            >
              <DeckRename data={data} decks={decks} setDecks={setDecks} />
              <DeckDeletion
                data={data}
                faction={faction}
                removeDeck={removeDeck}
              />
            </Col>
          </Row>
          <Link
            to={'/deck-details'}
            className={'text-decoration-none text-body'}
            onClick={() => setSelectedDeck(data)}
          >
            <Row>
              <Col md={12}>
                <h4 className="text-white">
                  {data?.name?.length > 16
                    ? `${data?.name?.slice(0, 15)?.trim()?.concat('...')}`
                    : `${data?.name}`}
                </h4>
              </Col>
            </Row>
          </Link>
        </Card.Header>
        <Card.Body>
          <Row className="align-items-start">
            <Col md={4}>
              <h1 className="fw-light">{data?.cards?.length ?? 0}</h1>
            </Col>
            <Col md={8} className="d-flex justify-content-end">
              <div className="text-secondary">total cards</div>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </>
  )
}

export default DeckOverview
