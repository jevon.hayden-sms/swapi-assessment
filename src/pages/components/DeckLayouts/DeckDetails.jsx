import React, { useEffect, useState } from 'react'
import { Card, Col, Row } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import CardOverview from '../CardLayouts/CardOverview'
import DeckCreation from '../Popover/DeckCreation'
import DeckDeletion from '../Popover/DeckDeletion'
import Search from '../Search'
import SortToggle from '../toggleTypes/SortToggle'
import { getFaction } from '../../../features/SWAPI/deck/factionSlice'
import {
  getPeople,
  getAllPeople,
  reset,
} from '../../../features/SWAPI/swapiSlice'
import { Link } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'

const _ = require('lodash')

const DeckDetails = ({
  item,
  deckData,
  selectedDeck,
  cardsInDeck,
  decks,
  setDecks,
  removeCard,
  screen,
  localStorageDecks,
  transferCard,
}) => {
  //   console.log(cardsInDeck, 'this is decks on deckdetails')
  //   console.log(deckData, 'deckData on deckdetails')
  //   console.log(selectedDeck, 'selectedDeck on deckdetails')

  const [transferDeckName, setTransferDeckName] = useState()
  const [characters, setCharacters] = useState([])
  const [searchResults, setSearchResults] = useState([])

  const dispatch = useDispatch()
  // const dispatch = useDispatch()
  const { people, allPersons, isError, isLoading, message } = useSelector(
    (state) => state.swapi
  )

  const faction = useSelector((state) => state.faction)

  const [stopDispatch, setStopDispatch] = useState(false)
  useEffect(() => {
    if (faction !== null && !stopDispatch) {
      setStopDispatch(true)
      dispatch(getFaction())
    }
  }, [faction])

  const getRemainingDecks = async () => {
    let remainingDeck = localStorageDecks
      .map((x) => x)
      .filter((f) => f.id !== selectedDeck?.id)
    console.log(remainingDeck, 'this is the remaining decks')
    setTransferDeckName(remainingDeck)
  }

  useEffect(() => {
    if (localStorageDecks.length > 1) {
      getRemainingDecks()
      //   console.log(localStorageDecks, 'this is decks on card')
    }
  }, [selectedDeck?.id])

  useEffect(() => {
    const loadCharacters = []

    if (isError) {
      console.log(message)
    }
    if (!people) {
      console.log('Could not fetch')
    }

    if (people.length === 0) {
      dispatch(getPeople())
      dispatch(getAllPeople())
    }

    if (characters.length === 0) {
      setCharacters(() => {
        for (let index = 0; index < allPersons.length; index++) {
          //   console.log([...allPersons[index].results], 'message')
          loadCharacters.push.apply(loadCharacters, [
            ...allPersons[index].results,
          ])
          //   console.log(allPersons[index].results, 'idx')
        }
        return loadCharacters.splice(0, loadCharacters.length)
      })
    }

    /* return () => {
      dispatch(reset())
    } */
  }, [people, allPersons, isError, message, dispatch])

  // console.log(selectedDeck, 'selectedDeck')
  // console.log(deckData, 'deckData on details')
  // console.log(localStorageDecks, 'localStorageDecks')

  /* const removeCard = async (data) => {
    let updatedCard = cardsInDeck[0]
      .map((x) => x)
      .filter((f) => f.url !== data.url)
    console.log(updatedCard, 'updatedCard')
    localStorage.setItem('deckName', JSON.stringify(updatedCard))
    setDecks([...updatedCard])
  }
 */

  return (
    <>
      <Row className={screen === true ? '' : 'align-items-start'}>
        <Col xs lg={4} className='my-3'>
          <Search data={cardsInDeck[0]} setSearchResults={setSearchResults} />
        </Col>
        {screen === true ? (
          <>
            <Col className='d-flex align-items-center'>
              <SortToggle />
            </Col>
          </>
        ) : (
          <>
            <Col className='my-3'>
              <div className='d-flex justify-content-end'>
                <DeckCreation faction={faction.faction} />
              </div>
            </Col>
          </>
        )}
      </Row>
      {cardsInDeck[0] < 1 || cardsInDeck[0] === undefined ? (
        <Row>
          <Col>
            <div>
              No cards added to {selectedDeck?.name ?? `the selected deck`}.{' '}
              <br />
              <br />
              Please add a card to a deck by selecting the{' '}
              {/* <Link to={'/cards'} className='text-decoration-none text-body'> */}
              <strong>All Cards </strong>
              {/* </Link>{' '} */}
              button on the <strong>top left</strong> of the screen and click
              the{' '}
              <span style={{ marginLeft: '-3px' }}>
                <img src='\Interview Assets\miniPlusBtn.svg' alt='' />
              </span>
              button at the <strong>top right</strong> of a{' '}
              <strong>card</strong>.
            </div>
          </Col>
        </Row>
      ) : (
        <Row>
          {cardsInDeck[0]?.map((card) => (
            <Col sm={12} md={6} lg={3} className='card-width' key={card?.url}>
              <CardOverview
                transferDeckName={transferDeckName}
                card={card}
                comp={'deck'}
                decks={decks}
                setDecks={setDecks}
                removeCard={removeCard}
                transferCard={transferCard}
                isDeck={true}
              />
            </Col>
          ))}
          <ToastContainer
            position='top-right'
            autoClose={2500}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            toastClassName={'toast-secondary'}
            progressClassName={'toast-progress-bar'}
          />
        </Row>
      )}
    </>
  )
}

export default DeckDetails
