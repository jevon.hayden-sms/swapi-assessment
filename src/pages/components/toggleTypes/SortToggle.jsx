import React, { useState } from 'react'
import { Button, ButtonGroup, ToggleButton } from 'react-bootstrap'

const SortToggle = ({ data, setCharacters }) => {
  const [active, setActive] = useState(0)
  const alphabetSort = async () => {
    let sortedData = await data.sort((a, b) =>
      a.name > b.name ? 1 : b.name > a.name ? -1 : 0
    )
    setCharacters(sortedData)
    setActive(1)
  }

  const youngestSort = async () => {
    let sortedData = await data.sort((a, b) =>
      a.birth_year.localeCompare(b.birth_year, undefined, {
        numeric: true,
        sensitivity: 'base',
      })
    )
    setCharacters(sortedData)
    setActive(2)
  }

  const eldestSort = async () => {
    let sortedData = await data
      .sort((a, b) =>
        a.birth_year.localeCompare(b.birth_year, undefined, {
          numeric: true,
          sensitivity: 'base',
        })
      )
      .reverse()
    setCharacters(sortedData)
    setActive(3)
  }

  return (
    <>
      <Button
        className={`btn-border-secondary me-2 text-nowrap ${
          active === 1 ? `btn-light__light` : `btn-secondary__light`
        }`}
        size='sm'
        onClick={() => alphabetSort()}
      >
        A to Z
      </Button>
      <Button
        className={`btn-border-secondary me-2 ${
          active === 2 ? `btn-light__light` : `btn-secondary__light`
        }`}
        size='sm'
        onClick={() => youngestSort()}
      >
        Youngest
      </Button>
      <Button
        className={`btn-border-secondary ${
          active === 3 ? `btn-light__light` : `btn-secondary__light`
        }`}
        size='sm'
        onClick={() => eldestSort()}
      >
        Eldest
      </Button>
    </>
  )
}

export default SortToggle
