import React, { useState } from 'react'
import { InputGroup } from 'react-bootstrap'
import { GrDown } from 'react-icons/gr'

const DropdownOption = () => {
  //   const [sortDropdownOption, setSortDropdownOption] = useState('')
  const options = [
    { name: 'Homeworld', value: '1' },
    { name: 'Species', value: '2' },
    { name: 'Vehicle Count', value: '3' },
    { name: 'Starship Count', value: '4' },
  ]
  return (
    <>
      <label htmlFor="Sorting options" className="me-3">
        Sort by
      </label>
      <InputGroup size="sm" className="select-width">
        <select
          className="form-control select-area"
          placeholder="Recipient's username"
          aria-label="Recipient's username"
          aria-describedby="inputGroup-sizing-sm"
        >
          {options.map((option, idx) => (
            <option value={option.value} key={idx}>
              {option.name}
            </option>
          ))}
        </select>
        <InputGroup.Text
          id="inputGroup-sizing-sm"
          className="select-secondary-light-1"
        >
          <GrDown className="icon-white" />
        </InputGroup.Text>
      </InputGroup>
    </>
  )
}

export default DropdownOption
