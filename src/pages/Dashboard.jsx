import { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getPeople, getAllPeople, reset } from '../features/SWAPI/swapiSlice'
import { Row, Col, Button } from 'react-bootstrap'
import { HiPlus } from 'react-icons/hi'
import CardOverview from './components/CardLayouts/CardOverview'
import Search from './components/Search'
import Loader from './components/Loader'
import DropdownOption from './components/toggleTypes/DropdownOption'
import SortToggle from './components/toggleTypes/SortToggle'
import axios from 'axios'
import Card from './components/Screens/Card'
import Deck from './components/Screens/Deck'
import DeckCreation from './components/Popover/DeckCreation'
import { getFaction } from '../features/SWAPI/deck/factionSlice'

const Dashboard = ({
  item,
  screen,
  deckData,
  setSelectedDeck,
  removeDeck,
  localStorageDecks,
  setLocalStorageDecks,
}) => {
  const [characters, setCharacters] = useState([])
  const [searchResults, setSearchResults] = useState([])
  const [species, setSpecies] = useState([])
  //   const [localStorageDecks, setLocalStorageDecks] = useState([])

  const dispatch = useDispatch()
  const { people, allPersons, isError, isLoading, message } = useSelector(
    (state) => state.swapi,
  )

  const faction = useSelector((state) => state.faction)

  const [stopDispatch, setStopDispatch] = useState(false)
  useEffect(() => {
    if (faction !== null && !stopDispatch) {
      setStopDispatch(true)
      dispatch(getFaction())
    }
  }, [faction])

  useEffect(() => {
    const loadCharacters = []

    if (isError) {
      console.log(message)
    }
    if (!people) {
      console.log('Could not fetch')
    }

    if (people.length === 0) {
      dispatch(getPeople())
      dispatch(getAllPeople())
    }

    if (characters.length === 0) {
      setCharacters(() => {
        for (let index = 0; index < allPersons.length; index++) {
          //   console.log([...allPersons[index].results], 'message')
          loadCharacters.push.apply(loadCharacters, [
            ...allPersons[index].results,
          ])
          //   console.log(allPersons[index].results, 'idx')
        }
        return loadCharacters.splice(0, loadCharacters.length)
      })
    }

    /* return () => {
      dispatch(reset())
    } */
  }, [people, allPersons, isError, message, dispatch])

  item('')
  // console.log(characters, 'characters')
  //   console.log(screen, 'this is screen on dashboard')
  let SWAPI_API_SPECIES = 'https://swapi.dev/api/species/'

  const getSpeciesData = async () => {
    let allSpeciesName = []
    let stopRequest = false
    while (stopRequest === false) {
      let speciesName = await axios.get(SWAPI_API_SPECIES)
      if (speciesName.data.next !== null) {
        allSpeciesName.push.apply(allSpeciesName, speciesName.data.results)
      } else {
        allSpeciesName.push.apply(allSpeciesName, speciesName.data.results)
        stopRequest = true
      }
      SWAPI_API_SPECIES = speciesName.data.next
    }
    setSpecies(allSpeciesName)
  }

  useEffect(() => {
    if (species.length === 0) {
      getSpeciesData()
    }
  }, [species])

  //   console.log(characters, 'this is characters')

  //   let comp = 'card'

  /*   const getData = async () => {
    let x = JSON.parse(await localStorage.getItem('deckName'))
    console.log(x, 'this is x on card')
    setLocalStorageDecks([...x])
  }

  useEffect(() => {
    if (localStorageDecks.length < 1) {
      getData()
      console.log(localStorageDecks, 'this is decks on card')
    }
  }, [localStorageDecks]) */

  // console.log(species, 'species')
  // console.log(characters.species, 'character species')

  /* // Retrieve information from link in person details
  const getLinkData = async (url) => {
    let newData = []
    for (let index = 0; index < url.length; index++) {
      let response = await axios.get(url[index])
      newData.push(response.data.name)
    }
    return newData
  }
  useEffect(() => {
    dataStructure()
  })
  // Restructure the data object
  const dataStructure = async () => {
    let data = characters
    console.log(data, 'data')
    if (characters.species !== null) {
      let specie = await getLinkData(characters.species)
      console.log(specie, 'species')
      characters.species = specie
    }
    setCharacters(data)
  } */

  //   console.log(characters, 'characters on dashboard')

  return (
    <>
      <Row className={screen === true ? '' : 'align-items-start'}>
        <Col xs lg={4} className="my-3">
          <Search data={characters} setSearchResults={setSearchResults} />
        </Col>
        {screen === true ? (
          <>
            <Col className="d-flex align-items-center">
              <SortToggle
                data={[...characters]}
                setCharacters={setCharacters}
              />
            </Col>
          </>
        ) : (
          <>
            <Col className="my-3">
              <div className="d-flex justify-content-end">
                {/*<Button variant='link'>
                   <HiPlus /> 
                  <img src='\Interview Assets\plusBtn.svg' alt='' />
        </Button>*/}
                <DeckCreation
                  deck={[...localStorageDecks]}
                  setDeck={setLocalStorageDecks}
                  faction={faction.faction}
                />
              </div>
            </Col>
          </>
        )}
      </Row>
      <Row>
        {screen ? (
          <Card
            characters={characters}
            species={species}
            searchResults={searchResults}
            decks={[...localStorageDecks]}
            setDecks={setLocalStorageDecks}
            allPersons={allPersons}
          />
        ) : (
          <Deck
            faction={faction.faction}
            deckData={[...deckData]}
            removeDeck={removeDeck}
            setSelectedDeck={setSelectedDeck}
            decks={[...localStorageDecks]}
            setDecks={setLocalStorageDecks}
          />
        )}
      </Row>
    </>
  )
}

export default Dashboard
