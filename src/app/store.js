import { configureStore } from '@reduxjs/toolkit'
import swapiReducer from '../features/SWAPI/swapiSlice'
import factionReducer from '../features/SWAPI/deck/factionSlice'

export const store = configureStore({
  reducer: {
    swapi: swapiReducer,
    faction: factionReducer,
  },
})
