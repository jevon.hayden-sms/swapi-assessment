import axios from 'axios'
import { v4 as uuidv4 } from 'uuid'

let SWAPI_API = 'https://swapi.dev/api/people/'
// let SWAPI_API_HOMEWORLD = 'https://swapi.dev/api/planets/:id/'
// let SWAPI_API_VEHICLE = 'https://swapi.dev/api/vehicles/'
// let SWAPI_API_STARSHIP = 'https://swapi.dev/api/starships/'
// let SWAPI_API_SPECIES = 'https://swapi.dev/api/species/'

// Get characters by page
const getPeople = async () => {
  const response = await axios.get(SWAPI_API)

  return response.data
}

// Get all characters and push to array
const getAllPeople = async () => {
  const allCharacters = []
  let stopRequest = false
  while (stopRequest === false) {
    let allCharactersPerPage = await getPeople()
    if (allCharactersPerPage.next !== null) {
      allCharacters.push(allCharactersPerPage)
    } else {
      allCharacters.push(allCharactersPerPage)
      stopRequest = true
    }

    SWAPI_API = allCharactersPerPage.next
  }

  return allCharacters
}

const swapiService = {
  getPeople,
  getAllPeople,
}

export default swapiService
