import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import swapiService from './swapiService'

const initialState = {
  people: [],
  allPersons: [],
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: '',
}

// AsyncThunk function to handle asynchronous data

export const getPeople = createAsyncThunk(
  'swapi/getPersons',
  async (thunkAPI) => {
    try {
      return await swapiService.getPeople()
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString()
      return thunkAPI.rejectWithValue(message)
    }
  },
)

export const getAllPeople = createAsyncThunk(
  'swapi/getAllPersons',
  async (thunkAPI) => {
    try {
      return await swapiService.getAllPeople()
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString()
      return thunkAPI.rejectWithValue(message)
    }
  },
)

export const swapiSlice = createSlice({
  name: 'swapi',
  initialState,
  reducers: {
    reset: (state) => initialState,
  },
  extraReducers: (builder) => {
    builder
      .addCase(getPeople.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getPeople.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.people.push(action.payload)
      })
      .addCase(getPeople.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.message = action.payload
      })
      .addCase(getAllPeople.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getAllPeople.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.allPersons = action.payload
      })
      .addCase(getAllPeople.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.message = action.payload
      })
  },
})

export const { reset } = swapiSlice.actions

export default swapiSlice.reducer
