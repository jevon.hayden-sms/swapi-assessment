const faction = {
  none: {
    id: 0,
    name: 'No Faction',
    watermark: `\\Interview Assets\\No Faction Watermark.svg`,
    icon: `\\Interview Assets\\No Faction.svg`,
    card_class: 'dark',
  },
  rebel: {
    id: 1,
    name: 'Rebel Alliance Deck',
    watermark: `\\Interview Assets\\Rebel Alliance Watermark.svg`,
    icon: `\\Interview Assets\\Rebel Alliance.svg`,
    card_class: 'danger',
  },
  jedi: {
    id: 2,
    name: 'Jedi Order Deck',
    watermark: `\\Interview Assets\\Jedi Order Watermark.svg`,
    icon: `\\Interview Assets\\Jedi Order.svg`,
    card_class: 'success',
  },
  galatic: {
    id: 3,
    name: 'Galatic Empire Deck',
    watermark: `\\Interview Assets\\Galatic Empire Watermark.svg`,
    icon: `\\Interview Assets\\Galatic Empire.svg`,
    card_class: 'dark',
  },
}

const factionService = {
  // getDecks,
  faction,
}

export default factionService
