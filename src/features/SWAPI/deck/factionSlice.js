import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import factionService from './factionService'

const initialState = {
  faction: [],
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: '',
}

export const getFaction = createAsyncThunk(
  'faction/getFaction',
  async (thunkAPI) => {
    try {
      return await factionService.faction
    } catch (error) {
      const message =
        (error.response && error.response.data && error.data.message) ||
        error.message ||
        error.toString()
      return thunkAPI.rejectWithValue(message)
    }
  }
)

export const factionSlice = createSlice({
  name: 'faction',
  initialState,
  reducers: {
    reset: (state) => initialState,
  },
  extraReducers: (builder) => {
    builder
      .addCase(getFaction.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getFaction.fulfilled, (state, action) => {
        state.isLoading = false
        state.isSuccess = true
        state.faction = action.payload
      })
      .addCase(getFaction.rejected, (state, action) => {
        state.isLoading = false
        state.isError = true
        state.message = action.payload
      })
  },
})

export const { reset } = factionSlice.actions

export default factionSlice.reducer
